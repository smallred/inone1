package main

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/golang-jwt/jwt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"golang.org/x/crypto/bcrypt"
	appsv1 "k8s.io/api/apps/v1"
	apiv1 "k8s.io/api/core/v1"
	v1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	yaml2 "k8s.io/apimachinery/pkg/util/yaml"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/tools/clientcmd"
)

var (
	registerCollection  *mongo.Collection
	orderCollection     *mongo.Collection
	activeCollection    *mongo.Collection
	errorinfoCollection *mongo.Collection
	Ctx                 context.Context
	clientset           *kubernetes.Clientset
)

type JWTCustomClaims struct {
	Username string `json:"username"`
	jwt.StandardClaims
}

var secrectKey = []byte("caiduimimasuannilihai")

func main() {
	// Initialize the Clientset for kubernetes
	kubeconfig := "/root/.kube/config"
	config, err := clientcmd.BuildConfigFromFlags("", kubeconfig)
	if err != nil {
		log.Fatal(err)
	}
	clientset, err = kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatal(err)
	}

	// Initialize the client for MongoDB
	client, err := mongo.NewClient(options.Client().ApplyURI(os.Getenv("ATLAS_URI")))
	if err != nil {
		log.Fatal(err)
	}
	Ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = client.Connect(Ctx)
	if err != nil {
		log.Fatal(err)
	}
	defer client.Disconnect(Ctx)
	err = client.Ping(Ctx, readpref.Primary())
	if err != nil {
		log.Fatal(err)
	}
	inOneDB := client.Database("inOne")
	registerCollection = inOneDB.Collection("register")
	orderCollection = inOneDB.Collection("order")
	activeCollection = inOneDB.Collection("active")
	fmt.Println("The MongoDB has been connected")

	router := gin.Default()
	router.LoadHTMLFiles("static/index.html", "static/login.html", "static/signup.html", "dashboard.html")
	router.StaticFS("/static", http.Dir("./static"))
	router.GET("/", func(ctx *gin.Context) {
		ctx.HTML(http.StatusOK, "index.html", gin.H{
			"description": "ok",
		})
	})
	router.POST("/login", loginRoute)

	auth := router.Group("/auth", JWTAuthMiddleWare)
	{
		auth.GET("/dashboard", func(c *gin.Context) {
			username, ok := c.Get("username")
			if !ok {
				c.JSON(http.StatusOK, gin.H{
					"error":       "error",
					"description": "Unauthenticated",
				})
				return

			}
			c.HTML(http.StatusOK, "dashboard.html", gin.H{
				"username":    username,
				"description": "ok",
			})
		})
		auth.GET("/order", queryOrder)
		auth.POST("/createRS", queryAcitve, createResourcesRoute)
		auth.POST("/deleteRS", queryAcitve, deleteResourcesRoute)
	}

	router.POST("/signup", signupRoute)

	router.Run(":8080")
}

func signupRoute(c *gin.Context) {
	username := c.PostForm("username")
	err := registerCollection.FindOne(Ctx, bson.M{"username": username}).Err()
	if err == nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": "Username already exists",
		})
		return
	}
	email := c.PostForm("email")
	password, err := hashPassword(c.PostForm("password"))
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": err.Error()})
		return
	}

	_, err = registerCollection.InsertOne(Ctx, bson.D{
		{"username", username},
		{"email", email},
		{"password", password},
	})
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": err.Error()})
		return
	} else {
		_, err = createNamespace(clientset, username)
		if err != nil {
			_, err = registerCollection.DeleteOne(Ctx, bson.M{"username": username, "email": email})
			if err != nil {
				c.JSON(http.StatusOK, gin.H{
					"status":  "error",
					"message": err.Error(),
				})
				return
			}
			c.JSON(http.StatusOK, gin.H{
				"status":  "error",
				"message": err.Error(),
			})
			return
		}
		c.Redirect(http.StatusMovedPermanently, "/static/login.html")
		return
	}

}

func loginRoute(c *gin.Context) {
	email := c.PostForm("inputEmail")
	password := c.PostForm("inputPassword")
	var validPassword bson.M
	err := registerCollection.FindOne(Ctx, bson.M{"email": email}).Decode(&validPassword)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": "Username not found",
		})
		return
	}
	if !comparePassword(password, validPassword["password"].(string)) {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": "Password is incorrect",
		})
		return
	}

	token, _ := generateToken(JWTCustomClaims{validPassword["username"].(string), jwt.StandardClaims{
		ExpiresAt: time.Now().Add(time.Hour * 6).Unix(),
	}})
	c.SetCookie("jwt", token, 3600*6, "", "", false, true)
	c.Redirect(http.StatusMovedPermanently, "/auth/dashboard")

}

func JWTAuthMiddleWare(c *gin.Context) {
	token, err := c.Cookie("jwt")
	if err != nil {
		c.Redirect(http.StatusMovedPermanently, "/static/login.html")
		c.Abort()
		return
	}
	claims, err := parseToken(token)
	if err != nil {
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": "Token is invalid"})
		c.Abort()
		return
	}
	c.Set("username", claims.Username)
	c.Next()

}

func hashPassword(password string) (string, error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(bytes), err
}

func comparePassword(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func generateToken(claims JWTCustomClaims) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	return token.SignedString([]byte(secrectKey))
}

func parseToken(signToken string) (*JWTCustomClaims, error) {
	var claims JWTCustomClaims
	token, err := jwt.ParseWithClaims(signToken, &claims, func(t *jwt.Token) (interface{}, error) {
		return secrectKey, nil
	})
	if token.Valid {
		return &claims, nil
	} else if ve, ok := err.(*jwt.ValidationError); ok {
		if ve.Errors&jwt.ValidationErrorMalformed != 0 {
			return nil, errors.New("invalid token")
		} else if ve.Errors&(jwt.ValidationErrorExpired|jwt.ValidationErrorNotValidYet) != 0 {
			return nil, errors.New("expried token")
		} else {
			fmt.Println("Couldn't handle this token:", err)
			return nil, errors.New("unexpected error")
		}
	} else {
		return nil, errors.New("unexpected error")
	}

}

func createNamespace(clientset *kubernetes.Clientset, username string) (string, error) {
	namespaceClient := clientset.
		CoreV1().
		Namespaces()

	// 实例化一个数据结构
	namespace := &apiv1.Namespace{
		ObjectMeta: metav1.ObjectMeta{
			Name: username,
		},
	}

	result, err := namespaceClient.Create(context.TODO(), namespace, metav1.CreateOptions{})

	if err != nil {
		return "", err
	} else {
		return result.GetName(), nil
	}
}

func createDeployment(clientset *kubernetes.Clientset, username string, image string) (string, error) {
	deploymentClient := clientset.
		AppsV1().
		Deployments(username)
	// Read the yaml file
	deployYaml, err := ioutil.ReadFile(image + "-deployment.yaml")
	if err != nil {
		return "", err
	}

	deployJson, err := yaml2.ToJSON(deployYaml)
	if err != nil {
		return "", err
	}
	deployment := &appsv1.Deployment{}
	err = json.Unmarshal(deployJson, deployment)
	if err != nil {
		return "", err
	}
	result, err := deploymentClient.Create(context.TODO(), deployment, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}
	return result.GetName(), nil
}

func queryOrder(c *gin.Context) {
	username, exist := c.Get("username")
	if !exist {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  "error",
			"message": "Unauthenticated",
		})
		return
	}
	cursor, err := orderCollection.Find(Ctx, bson.M{"username": username})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Query order failed",
		})
		return
	}
	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Query order failed",
		})
		return
	}
	c.Set("orders", results)
	c.Next()

}

func queryAcitve(c *gin.Context) {
	username, exist := c.Get("username")
	if !exist {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  "error",
			"message": "Unauthenticated",
		})
		return
	}
	cursor, err := activeCollection.Find(Ctx, bson.M{"username": username})
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Query active failed",
		})
		return
	}
	var results []bson.M
	if err = cursor.All(context.TODO(), &results); err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Query active failed",
		})
		return
	}
	c.Set("active", results)
	c.Next()
}

func createService(clientset *kubernetes.Clientset, username string, image string) (string, error) {
	serviceClient := clientset.CoreV1().Services(username)
	seriveYaml, err := ioutil.ReadFile(image + "-service.yaml")
	if err != nil {
		return "", err
	}
	serviceJson, err := yaml2.ToJSON(seriveYaml)
	if err != nil {
		return "", err
	}
	service := &apiv1.Service{}
	err = json.Unmarshal(serviceJson, service)
	if err != nil {
		return "", err
	}
	result, err := serviceClient.Create(context.TODO(), service, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}
	return result.GetName(), nil

}

func createIngress(clientset *kubernetes.Clientset, username string, image string) (string, error) {
	//ingressClient := clientset.ExtensionsV1beta1().Ingresses(username)
	ingressClient := clientset.NetworkingV1().Ingresses(username)
	ingressYaml, err := ioutil.ReadFile(image + "-ingress.yaml")
	if err != nil {
		return "", err
	}
	ingressJson, err := yaml2.ToJSON(ingressYaml)
	if err != nil {
		return "", err
	}
	ingress := &v1.Ingress{}
	err = json.Unmarshal(ingressJson, ingress)
	if err != nil {
		return "", err
	}
	ingress.Spec.Rules[0].Host = username + "." + image + ".mssz.xyz"
	result, err := ingressClient.Create(context.TODO(), ingress, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}
	return result.GetName(), nil
}

func createPVC(clientset *kubernetes.Clientset, username string, image string) (string, error) {
	pvcClient := clientset.CoreV1().PersistentVolumeClaims(username)
	pvcYaml, err := ioutil.ReadFile(image + "-pvc.yaml")
	if err != nil {
		return "", err
	}
	pvcJson, err := yaml2.ToJSON(pvcYaml)
	if err != nil {
		return "", err
	}
	pvc := &apiv1.PersistentVolumeClaim{}
	err = json.Unmarshal(pvcJson, pvc)
	if err != nil {
		return "", err
	}
	result, err := pvcClient.Create(context.TODO(), pvc, metav1.CreateOptions{})
	if err != nil {
		return "", err
	}
	return result.GetName(), nil
}

func deleteDeployment(clientset *kubernetes.Clientset, username string, image string) error {
	deploymentClient := clientset.
		AppsV1().
		Deployments(username)

	err := deploymentClient.Delete(context.TODO(), image+"-deployment", metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}

func deleteService(clientset *kubernetes.Clientset, username string, image string) error {
	serviceClient := clientset.CoreV1().Services(username)
	err := serviceClient.Delete(context.TODO(), image+"-service", metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}

func deleteIngress(clientset *kubernetes.Clientset, username string, image string) error {
	ingressClient := clientset.NetworkingV1().Ingresses(username)
	err := ingressClient.Delete(context.TODO(), image+"-ingress", metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}

func deletePVC(clientset *kubernetes.Clientset, username string, image string) error {
	pvcClient := clientset.CoreV1().PersistentVolumeClaims(username)
	err := pvcClient.Delete(context.TODO(), image+"-pvc", metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	return nil
}

func createResourcesRoute(c *gin.Context) {
	image := c.PostForm("image")
	activeList, exist := c.Get("active")
	if !exist {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Cannot get active list",
		})
		return
	}
	username, exist := c.Get("username")
	if !exist {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Cannot get active list",
		})
		return
	}
	for _, active := range activeList.([]bson.M) {
		activeImage := active["image"].(string)
		if activeImage == image {
			c.AbortWithStatusJSON(http.StatusOK, gin.H{
				"status":  "error",
				"message": "Image already exists",
			})
			return
		}
	}
	_, err := createDeployment(clientset, username.(string), image)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Create deployment failed",
		})
		return
	}
	_, err = createService(clientset, username.(string), image)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Create service failed",
		})
		return
	}
	_, err = createPVC(clientset, username.(string), image)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Create pvc failed",
		})
		return
	}
	_, err = createIngress(clientset, username.(string), image)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Create ingress failed",
		})
		return
	}
	activeCollection.InsertOne(Ctx, bson.D{
		{"image", image},
		{"username", username},
		{"start", time.Now().Unix()},
		{"end", time.Now().Add(time.Hour * 2).Unix()},
	})
	//添加返回值
	c.JSON(http.StatusOK, gin.H{
		"status":  "success",
		"message": "Create resources success",
	})
	return

}

func deleteResourcesRoute(c *gin.Context) {
	numerr := 0
	image := c.PostForm("image")
	username, exist := c.Get("username")
	if !exist {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"status":  "error",
			"message": "Unauthenticated",
		})
		return
	}
	activeList, exist := c.Get("active")
	if !exist {
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{
			"status":  "error",
			"message": "Cannot get active list",
		})
		return
	}
	for _, active := range activeList.([]bson.M) {
		activeImage := active["image"].(string)
		if activeImage == image {
			// c.AbortWithStatusJSON(http.StatusOK, gin.H{
			// 	"status":  "error",
			// 	"message": "Image already exists",
			// })
			// return
			err := deleteDeployment(clientset, username.(string), image)
			if err != nil {
				numerr = 1
			}
			deleteService(clientset, username.(string), image)
			deleteIngress(clientset, username.(string), image)
			deletePVC(clientset, username.(string), image)
			activeCollection.DeleteOne(Ctx, bson.D{
				{"image", image},
				{"username", username},
			})
			if numerr != 0 {
				errorinfoCollection.InsertOne(Ctx, bson.D{
					{"image", image},
					{"username", username},
					{"time", time.Now()},
					{"error", err.Error()},
				})
			}
			c.JSON(http.StatusOK, gin.H{
				"status":  "success",
				"message": "Delete resources success",
			})
			return
		}
		c.JSON(http.StatusOK, gin.H{
			"status":  "error",
			"message": "Image not found",
		})
		return
	}
}
